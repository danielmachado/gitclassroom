import axios from 'axios'
import { baseApiUrl, getCode } from "@/global";

export default class ClassService {
    static create(klass, members) {
        return axios.post(`${baseApiUrl}/api/classroom`, { code: getCode(), ...klass, members })
    }

    static all() {
        return axios.get(`${baseApiUrl}/api/classroom?code=${getCode()}`)
    }

    static get(id) {
        return axios.get(`${baseApiUrl}/api/classroom/${id}/?code=${getCode()}`)
    }

    static delete(id) {
        return axios.delete(`${baseApiUrl}/api/classroom/${id}/?code=${getCode()}`)
    }

    static assignments(id, filter_by = 'OPENED') {
        return axios.get(`${baseApiUrl}/api/classroom/${id}/assignments?code=${getCode()}&filter_by=${filter_by}`)
    }

    static addMember(classroom_external_id, members) {
        return axios.post(`${baseApiUrl}/api/classroom/members`, { code: getCode(), classroom_external_id, members })
    }

    static removeMember(classroom_external_id, members) {
        return axios.delete(`${baseApiUrl}/api/classroom/${classroom_external_id}/members?code=${getCode()}&members=${members}`)
    }

    static sendEmail(email) {
        return axios.get(`${baseApiUrl}/api/invite/?email=${email}`)
    }

    static validateEmail(email) {
        return /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/.test(email);
    }

    static onlyNames(classes) {
        let array = [];
        classes.forEach(element => {
            array.push(element.name)
        });
        return array;
    }
}