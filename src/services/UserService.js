import axios from 'axios'
import { baseApiUrl, getCode } from "@/global";

export default class UserService {
    static auth(code) {
        return axios.post(`${baseApiUrl}/api/user/auth`, { code })
    }

    static register(body) {
        return axios.post(`${baseApiUrl}/api/user/register`, { ...body })
    }

    static update(body) {
        return axios.put(`${baseApiUrl}/api/user/current`, { ...body, code: getCode() })
    }

    static getBy(value, type = "registration_number") {
        return axios.get(`${baseApiUrl}/api/user?code=${getCode()}&${type}=${value}`)
    }

    static generateHash() {
        return '_' + Math.random().toString(36).substr(2, 9)
    }

    static validateEmail(email) {
        return /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/.test(email);
    }
}