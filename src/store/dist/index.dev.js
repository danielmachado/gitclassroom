"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _vuex = _interopRequireDefault(require("vuex"));

var _classes = _interopRequireDefault(require("@/store/modules/classes"));

var _user = _interopRequireDefault(require("@/store/modules/user"));

var _task = _interopRequireDefault(require("@/store/modules/task"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_vue["default"].use(_vuex["default"]);

var _default = new _vuex["default"].Store({
  state: {
    darkMode: true
  },
  mutations: {
    toggleTheme: function toggleTheme(state) {
      state.darkMode = !state.darkMode;
    }
  },
  actions: {},
  modules: {
    classes: _classes["default"],
    user: _user["default"],
    task: _task["default"]
  }
});

exports["default"] = _default;