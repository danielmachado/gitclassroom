export default {
    namespaced: true,
    state: {
        modal: false,
        task: {},
        openTips: false,
        students: [],
        showCommit: false,
    },
    mutations: {
        setTask(state, task) {
            state.task = task
        },
        setStudents(state, students) {
            state.students = students
        },
        setModal(state, boolean = true) {
            state.modal = boolean
        },
        setTips(state) {
            state.openTips = !state.openTips
        },
        setshowCommit(state, boolean = true) {
            state.showCommit = boolean;
        }
    },
    actions: {
        getTask({ commit }, task) {
            commit('setTask', task);
        }
    }
}