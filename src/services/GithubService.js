import axios from 'axios'

export default class GithubService {
  static markdown(repository) {
    return axios.get(`${repository}/raw/master/README.md`, {
      headers: { "Access-Control-Allow-Origin": "*" }
    })
  }
}