import ClassService from "@/services/ClassService";

export default {
    namespaced: true,
    state: {
        modal: false,
        modalStudent: false,
        oneClass: {},
        members: [],
        classes: [],
        alert: false
    },
    mutations: {
        setClasses(state, classes) {
            state.classes = classes
        },
        setClass(state, payload) {
            state.oneClass = payload;
        },
        setModal(state, boolean = true) {
            state.modal = boolean
        },
        setAlert(state, boolean = true) {
            state.alert = boolean;
        },
        setMembers(state, members) {
            state.members = members
        },
        setModalStudent(state, boolean = true) {
            state.modalStudent = boolean
        },
        setNewStudentModal(state, modal = true) {
            state.newStudentModal = modal;
        },
        pushUserToClass(state, user) {
            state.members.push(user);
        }
    },
    actions: {
        getClass({ commit }, klass) {
            commit('setClass', klass);
        },
        setAssignments({ commit, state }, assignments) {
            commit('setClass', { ...state.oneClass, assignments })
        },
        createClass({ commit, state }, body) {
            commit('setClass', body)
            if (!state.oneClass.avatar_url)
                commit('setClass', {
                    avatar_url: 'https://picsum.photos/200/300',
                    ...body
                })
            let membersName = state.members.reduce((prevVal, elem) => (elem ? prevVal + elem.username + ',' : ''), '')
            membersName = membersName.slice(0, -1);
            return ClassService.create(state.oneClass, membersName)
        },
    },
    getters: {
        usersQuantity(state) {
            return state.members.length;
        },
        onlyNameMembersString(state) {
            return state.members.reduce((prevVal, elem) => prevVal + elem.username, '')
        }
    }
}