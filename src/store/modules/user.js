import axios from 'axios'

export default {
    namespaced: true,
    state: {
        user: null,
        viewModal: false,
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
            if (user)
                axios.defaults.headers.common['Authorization'] = `Bearer ${user.code}`;
            else
                delete axios.defaults.headers.common['Authorization'];
        },
        toggleModal(state) {
            state.viewModal = !state.viewModal
        }
    },
}