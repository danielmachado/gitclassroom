import Vue from 'vue'
import Vuex from 'vuex'

import classes from "@/store/modules/classes";
import user from "@/store/modules/user";
import task from "@/store/modules/task";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        darkMode: true,
    },
    mutations: {
        toggleTheme(state) {
            state.darkMode = !state.darkMode;
        },
        setTheme(state, theme) {
            state.darkMode = theme;
        }
    },
    actions: {},
    modules: {
        classes,
        user,
        task
    }
})
