import UserService from './UserService'
import ClassService from './ClassService'
import TaskService from './TaskService'
import Color from './Color'

export {
  ClassService,
  UserService,
  TaskService,
  Color
}