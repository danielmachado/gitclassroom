import Main from "./Main";
import Loading from "./Loading";
import Menu from "./Menu";
import PageTitle from "./PageTitle";

export {
    Main,
    Loading,
    Menu,
    PageTitle,
}