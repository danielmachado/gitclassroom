import axios from 'axios'
import { baseApiUrl, getCode } from "@/global";

export default class TaskService {
    static create(task) {
        return axios.post(`${baseApiUrl}/api/assignment`, { code: getCode(), ...task })
    }

    static accept(id) {
        return axios.post(`${baseApiUrl}/api/assignment/${id}/accept`, { code: getCode() })
    }

    static students(id) {
        return axios.get(`${baseApiUrl}/api/assignment/${id}/students/?code=${getCode()}`)
    }

    static isAlreadyAccept(id) {
        return axios.get(`${baseApiUrl}/api/assignment/${id}/is_accepted/?code=${getCode()}`)
    }
    
    static getREADME(repository_url){
        return axios.get(`${baseApiUrl}/api/readme/?repository_url=${repository_url}`)
    }

    static getAllGrades(id){
        return axios.get(`${baseApiUrl}/api/assignment/${id}/grades/?code=${getCode()}`)
    }

    static delete(id) {
        return axios.delete(`${baseApiUrl}/api/assignment/${id}/?code=${getCode()}`)
    }

    static close(id) {
        return axios.get(`${baseApiUrl}/api/assignment/${id}/close?code=${getCode()}`)
    }

    static post_grade(id, grades) {
        return axios.post(`${baseApiUrl}/api/assignment/${id}/grade`, { code: getCode(), ...grades })
    }

    static get(id) {
        return axios.get(`${baseApiUrl}/api/assignment/${id}/?code=${getCode()}`)
    }
}